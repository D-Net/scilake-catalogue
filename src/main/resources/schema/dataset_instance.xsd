<?xml version="1.0" encoding="utf-8" ?>

<!--
Changes in v0.0.3

 - In preparation for the tools, the "location" element is no longer mandatory
 - Added an id

-->

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ins="http://datacite.org/schema/kernel-4" elementFormDefault="qualified" xml:lang="EN">
    <xs:import namespace="http://datacite.org/schema/kernel-4" schemaLocation="imports.xsd"/>
    <xs:element name="dataset_instance">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="id" type="xs:string" minOccurs="1" maxOccurs="1" />
                <xs:element name="type" type="xs:string" />
                <xs:element name="metadata" type="metadataType" />
                <xs:element name="location" minOccurs="0" maxOccurs="1">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="files" minOccurs="0" maxOccurs="1">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="baseDir" type="xs:string" />
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="database" minOccurs="0" maxOccurs="1">
                                <xs:complexType>
                                    <xs:all>
                                        <xs:element name="type" type="xs:string" />
                                        <xs:element name="connection_url" type="xs:string" />
                                        <xs:element name="schema" type="xs:string" />
                                    </xs:all>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:complexType name="metadataType">
        <xs:all>
            <xs:annotation>
                <xs:documentation>The following metadata elements are based on the DataCite schema</xs:documentation>
            </xs:annotation>
            <xs:element name="version" type="xs:string" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Version number of the resource. If the primary resource has changed the version number increases.</xs:documentation>
                    <xs:documentation>Register a new identifier for a major version change. Individual stewards need to determine which are major vs. minor versions. May be used in conjunction with properties 11 and 12 (AlternateIdentifier and RelatedIdentifier) to indicate various information updates. May be used in conjunction with property 17 (Description) to indicate the nature and file/record range of version.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="identifier" minOccurs="0">
                <xs:complexType>
                    <xs:simpleContent>
                        <xs:extension base="xs:string">
                            <xs:attribute name="identifierType" type="xs:string" use="required" />
                        </xs:extension>
                    </xs:simpleContent>
                </xs:complexType>
            </xs:element>
            <xs:element name="creators">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="creator" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>The main researchers involved working on the data, or the authors of the publication in priority order. May be a corporate/institutional or personal name.</xs:documentation>
                                <xs:documentation>Format: Family, Given.</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="creatorName" type="xs:string" />
                                    <xs:element name="nameIdentifier" minOccurs="0">
                                        <xs:complexType>
                                            <xs:simpleContent>
                                                <xs:extension base="xs:string">
                                                    <xs:attribute name="nameIdentifierScheme" use="required" />
                                                    <xs:attribute name="schemeURI" type="xs:anyURI" use="optional" />
                                                </xs:extension>
                                            </xs:simpleContent>
                                        </xs:complexType>
                                    </xs:element>
                                    <xs:element name="affiliation" minOccurs="0" maxOccurs="unbounded" />
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="contributors" minOccurs="0">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="contributor" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>The institution or person responsible for collecting, creating, or otherwise contributing to the developement of the dataset.</xs:documentation>
                                <xs:documentation>The personal name format should be: Family, Given.</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="contributorName" type="xs:string" />
                                    <xs:element name="nameIdentifier" minOccurs="0">
                                        <xs:complexType>
                                            <xs:simpleContent>
                                                <xs:extension base="xs:string">
                                                    <xs:attribute name="nameIdentifierScheme" use="required" />
                                                    <xs:attribute name="schemeURI" type="xs:anyURI" use="optional" />
                                                </xs:extension>
                                            </xs:simpleContent>
                                        </xs:complexType>
                                    </xs:element>
                                    <xs:element name="affiliation" minOccurs="0" maxOccurs="unbounded" />
                                </xs:sequence>
                                <xs:attribute name="contributorType" type="ins:contributorType" use="required" />
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="dates" minOccurs="0">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="date" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Different dates relevant to the work.</xs:documentation>
                                <xs:documentation>YYYY,YYYY-MM-DD, YYYY-MM-DDThh:mm:ssTZD or any other format or level of granularity described in W3CDTF. Use RKMS-ISO8601 standard for depicting date ranges.</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:simpleContent>
                                    <xs:extension base="xs:string">
                                        <xs:attribute name="dateType" type="ins:dateType" use="required" />
                                    </xs:extension>
                                </xs:simpleContent>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="sizes" minOccurs="0">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="size" type="xs:string" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Unstructures size information about the resource.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:all>
    </xs:complexType>
</xs:schema>
