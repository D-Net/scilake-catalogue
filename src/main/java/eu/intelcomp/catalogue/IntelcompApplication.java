package eu.intelcomp.catalogue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntelcompApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntelcompApplication.class, args);
    }

}
