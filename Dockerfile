### Build using Maven ###
FROM maven:3.8-jdk-11-slim AS maven

COPY pom.xml /tmp/
COPY . /tmp/

WORKDIR /tmp/

## run maven package ##
RUN mvn package -U -DskipTests


FROM openjdk:11
RUN mkdir -p /etc/intelcomp /opt/intelcomp
COPY --from=maven /tmp/target/*.jar /opt/intelcomp/intelcomp-catalogue.jar
# COPY application.yml /etc/intelcomp/application.yml
RUN useradd -l -m -s /bin/bash -N -u 1000 intelcomp
WORKDIR /opt/intelcomp
# ENTRYPOINT ["java","-jar","/opt/intelcomp/intelcomp-catalogue.jar", "--spring.config.location=/etc/intelcomp/application.yml"]
USER intelcomp
ENTRYPOINT ["java","-jar","/opt/intelcomp/intelcomp-catalogue.jar"]